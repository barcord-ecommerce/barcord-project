# Project

Project Description

<em>[TODO.md spec & Kanban Board](https://bit.ly/3fCwKfM)</em>

### Todo

- [ ] stock in button  
- [ ] make active link aside dynamic  
- [ ] add barcode to invoice  
- [ ] make all dash board work  
- [ ] export purchase receipt for supplier  
- [ ] Add Purchase from supplier  
- [ ] import data using xml ecel  

### In Progress

- [ ] quantity from product when doing pos  

### Done ✓

- [x] Add supplier  

