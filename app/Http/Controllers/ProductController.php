<?php

namespace App\Http\Controllers;

use App\Models\CategoryModel;
use Illuminate\Support\Facades\DB;
use App\Models\ProductModel;
use Illuminate\Http\Request;
use App\Models\ProductTableModel;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Models\SupplierModel;
use Illuminate\Support\Facades\Cache;
class ProductController extends Controller
{
    //
    public function add(Request $request)
    {
        $products = CategoryModel::get();

        $product_name = $request->product_name;
        $product_price = $request->product_price;
        $category_id = $request->category_id;
        $product_qty = $request->product_qty;
        $product_code = $request->product_code;
        $supplier_id = $request->supplier_id;

        $check_barcode = ProductModel::where([
            ['product_code', '=', $product_code],

        ])->first();
        if ($check_barcode) {
             $id = $check_barcode->id;

            return redirect()->back()->with(['barcode' => 'Product with this barcode already exists', 'product_code' => $product_code, 'id' => $id]);
            // return view('product.add_product', ['product_code' => $product_code]);
        }
        $data = array(
            'product_name' => $product_name, 'product_price' => $product_price,
            'category_id' => $category_id, 'product_qty' => $product_qty, 'product_code' => $product_code,
            'supplier_id' => $supplier_id
        );
        ProductModel::insert($data);
        return redirect()->back()->with(['message' => 'Successfully added the product ']);
        // return view('product.add_product', ['products' => $result]);
    }
    public function index()
    {
        $cart = Cart::content();
        $suppliers = SupplierModel::get();
        $result = ProductModel::get();
        return view('product.manage_product', ['products' => $result, 'cart' => $cart, 'suppliers' => $suppliers]);
    }
    public function destroy($id)
    {

        ProductTableModel::find($id)->delete();

        $result = ProductModel::get();
        return redirect()->back()->with(['message' => 'Successfully deleted the product ']);
    }
    public function view_product($id)
    {
        $category = CategoryModel::get();
        $suppliers = SupplierModel::get();

        // $product = ProductModel::select('*')->where('product_code', $product_code)->first();
       
        $product = ProductModel::select('*')->where('id', $id)->get();

        return view('product.view_product', ['products' => $product, 'category' => $category, 'suppliers' => $suppliers]);
    }
    public function edit(Request $request){
        $id = $request->id;
        $update = ProductTableModel::find($id);

        $product_name = $request->product_name;
        $product_price = $request->product_price;
        $product_qty = $request->product_qty;
        // $category_id = ;
        // dd($request->category_id);
        $product_code = $request->product_code;
        if ($request->file('product_change')){
            $image_path = public_path('upload/products/').$update->product_img; 
    
            if (file_exists($image_path)) {

                @unlink($image_path);

            }
            $file = $request->file('product_change');

            $filename = date('YmuidH').$file->getClientOriginalName();
            $file->move(public_path('upload/products'), $filename);
             $update['product_img'] = $filename;

            
        }
        $update->product_name = $product_name;
        $update->product_price = $product_price;
        $update->product_qty = $product_qty;
        $update->category_id = $request->category_id;
        $update->product_code = $product_code;
        $update->save();
        // $category = CategoryModel::all();

        // $product = ProductModel::select('*')->where('product_code', $product_code)->get();

        return redirect()->back()->with(['message' => 'Successfully edited the product ']);

    }
    public function grid_view(){
        $result = ProductModel::get();
        return view('product.grid_view', ['products' => $result]);

    }

    public function add_product(){
        $categories = CategoryModel::get();
     $suppliers = SupplierModel::get();
 // cate database  You may use the rememberForever method to retrieve an item from the cache or store it forever if it does not exist:
        //   $data=   cache()->rememberForever('bigx',function () {
        //         return DB::table('suppliers')->get();
        //     });
        //     return  $data;
    
// data for You may even pass a closure as the default value. The result of the closure will be returned if the specified item does not exist in the cache.
            // $value = Cache::get('key', function () {
            //     return DB::table('suppliers')->get();
            // });
            //   return $value;
            //  $data =Cache::add('key', 0, now()->addHours(4));

 // get cache useing put You may use the put method on the Cache facade to store items in the cache:
           // Cache::put('key',   $suppliers, now()->addHours(4));
           //   return Cache::get('key');

// Using the Cache facade, you may access various cache stores via the store method. The key passed to the store method should correspond to one of the stores listed in the stores configuration array in your cache configuration file:
            //   Cache::put('key','hello world');
            //     $value = Cache::store('file')->get('key');
            //      return $value;
 

//  Using the Cache facade, you may access various cache stores via the store method. The key passed to the store method should correspond to one of the stores listed in the stores configuration array in your cache configuration file:     
    //  Cache::put('key','hello world');
    //  $value = Cache::store('file')->get('key');
 
   // error this return ===> return  Cache::store('redis')->put('key','hello world'); // 10 Minutes
//  The increment and decrement methods may be used to adjust the value of integer items in the cache. Both of these methods accept an optional second argument indicating the amount by which to increment or decrement the item's value:
        //    Cache::put('key', 'hello world');
        //  $result   =  Cache::increment('key');
        //    return $result;
//  If you need to retrieve an item from the cache and then delete the item, you may use the pull method. Like the get method, null will be returned if the item does not exist in the cache:
        // Cache::put('key', 'hello world');
        // $result = Cache::pull('key');
        // return $result;
// You may use the put method on the Cache facade to store items in the cache:        
    //  Cache::put('key', 'hello world');
    // return    Cache::put('key','value', $seconds = 10);
// Instead of passing the number of seconds as an integer, you may also pass a DateTime instance representing the desired expiration time of the cached item:
//   Cache::put('key', 'value hell thon cache now', now()->addMinutes(10));
//  return  Cache::get('key');

// this topic ping now 
Cache::put('category',$categories);
Cache::put('suppliers',$suppliers);
$value = Cache::get('category','suppliers', function () {
                return view('product.add_product', compact('categories', 'suppliers')); // get data for cache get 
            });
         return view('product.add_product', compact('categories', 'suppliers'));
       //   return view('product.add_product', compact('categories', 'suppliers'));
//The forever method may be used to store an item in the cache permanently. Since these items will not expire, they must be manually removed from the cache using the forget method:
            //     Cache::put('key','hello thon');
            //    return Cache::forever('key', 'value');
// You may remove items from the cache using the forget method:
    //  return Cache::forget('key');

// You may clear the entire cache using the flush method:
    // Cache::flush();
    // return  Cache::pull('category');
//   If you provide an array of key / value pairs and an expiration time to the function, it will store values in the cache for the specified duration:
        //    cache(['category' => 'value'], now()->addSecond());
        //    return   Cache::pull('category');


     //
  
    }


}
